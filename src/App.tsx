import * as React from 'react';
import './App.css';
import Tile from './Tile';

const { Fragment, Component } = React;

const MAP_SIZE = 24;
const INITIAL_PLAYER_HISTORY: InterfacePosition[] = [
  { x: 3, y: 12 },
  { x: 3, y: 11 },
  { x: 3, y: 10 },
  { x: 3, y: 9 },
  { x: 3, y: 8 },
  { x: 3, y: 7 },
  { x: 3, y: 6 },
  { x: 3, y: 5 },
  { x: 3, y: 4 }
];

interface InterfacePosition {
  x: number;
  y: number;
}

interface InterfaceAppState {
  direction: string; // 'left' | 'right' | 'up' | 'down';
  isPlaying: boolean;
  board: number[][];
  playerPosition: InterfacePosition;
  playerLength: number;
  playerHistory: InterfacePosition[];
  applePosition: InterfacePosition;
  gameLost: number;
}

class App extends Component<{}, InterfaceAppState> {
  constructor(props: {}) {
    super(props);
    this.state = {
      applePosition: {
        x: 5,
        y: 10
      },
      board: new Array(MAP_SIZE)
        .fill(0)
        .map(array => new Array(MAP_SIZE).fill(0)),
      direction: 'up',
      gameLost: 0,
      isPlaying: false,
      playerHistory: INITIAL_PLAYER_HISTORY,
      playerLength: 2,
      playerPosition: {
        x: 2,
        y: 3
      }
    };
  }

  public refreshGame = () => {
    const { playerPosition, direction } = this.state;
    if (direction === 'left') {
      playerPosition.x -= 1;
    } else if (direction === 'right') {
      playerPosition.x += 1;
    } else if (direction === 'down') {
      playerPosition.y += 1;
    } else if (direction === 'up') {
      playerPosition.y -= 1;
    }
    if (playerPosition.x < 0) {
      playerPosition.x = MAP_SIZE - 1;
    }
    if (playerPosition.y < 0) {
      playerPosition.y = MAP_SIZE - 1;
    }
    if (playerPosition.x >= MAP_SIZE) {
      playerPosition.x = 0;
    }
    if (playerPosition.y >= MAP_SIZE) {
      playerPosition.y = 0;
    }
    this.movePlayer(playerPosition.x, playerPosition.y);
    setTimeout(() => {
      requestAnimationFrame(this.refreshGame);
    }, 100);
  };

  public handleOnKeyDown = (event: { code: string }) => {
    this.changeDirection(event.code.split('Arrow')[1].toLowerCase());
  };

  public changeDirection(direction: string) {
    if (direction === this.state.direction) {
      return;
    }
    const leftRight = direction === 'left' && this.state.direction === 'right';
    const rightLeft = direction === 'right' && this.state.direction === 'left';
    const upDown = direction === 'up' && this.state.direction === 'down';
    const downUp = direction === 'down' && this.state.direction === 'up';
    if (leftRight || rightLeft || upDown || downUp) {
      return;
    }
    this.setState(state => ({
      direction
    }));
  }

  public movePlayer = (x, y) => {
    this.setState(state => {
      if (this.isGameLost(state.playerHistory, state.playerPosition)) {
        return {
          applePosition: this.generateApple(),
          gameLost: state.gameLost + 1,
          playerHistory: INITIAL_PLAYER_HISTORY,
          playerPosition: { x: 3, y: 4 }
        };
      }
      const position = state.applePosition;
      const isEatingApple = position.x === x && position.y === y;
      const newPlayerHistory = [...state.playerHistory.slice(1), { x, y }];
      const newPlayerHistoryExtended = [...state.playerHistory, { x, y }];
      return {
        applePosition: isEatingApple
          ? this.generateApple()
          : state.applePosition,
        gameLost: state.gameLost,
        playerHistory: isEatingApple
          ? newPlayerHistoryExtended
          : newPlayerHistory,
        playerPosition: { x, y }
      };
    });
  };

  public isGameLost = (
    playerHistory: InterfacePosition[],
    playerPosition: InterfacePosition
  ) => {
    return playerHistory.find(historyPosition => {
      return (
        historyPosition.x === playerPosition.x &&
        historyPosition.y === playerPosition.y
      );
    });
  };

  public generateApple = () => {
    return {
      x: Math.round(Math.random() * (MAP_SIZE - 1)),
      y: Math.round(Math.random() * (MAP_SIZE - 1))
    };
  };
  public componentDidMount() {
    document.addEventListener('keydown', this.handleOnKeyDown);
    this.refreshGame();
  }

  public componentWillUnmount() {
    document.removeEventListener('keydown', this.handleOnKeyDown);
  }

  public render() {
    const { playerHistory, applePosition } = this.state;
    return (
      <Fragment>
        <h3 style={{ textAlign: 'center' }}>Snake 🐍</h3>
        <div>
          <p style={{ textAlign: 'center' }}>
            {' '}
            Lost game times: {this.state.gameLost}{' '}
          </p>
        </div>
        <div>
          {this.state.board.map((row, rowIndex) => {
            return (
              <div
                key={`index-${rowIndex}`}
                style={{
                  display: 'flex',
                  height: '1.5em',
                  justifyContent: 'center',
                  margin: 0,
                  padding: 0
                }}
              >
                {row.map((tile, columnIndex) => {
                  const isActive = playerHistory.find(position => {
                    return (
                      position.x === columnIndex && position.y === rowIndex
                    );
                  });
                  const isApple =
                    applePosition.x === columnIndex &&
                    applePosition.y === rowIndex;

                  const firstPosition = playerHistory[playerHistory.length - 1];
                  const isSnakeHead =
                    columnIndex === firstPosition.x &&
                    rowIndex === firstPosition.y;

                  return (
                    <Tile
                      active={isActive}
                      isApple={isApple}
                      isSnakeHead={isSnakeHead}
                      key={`${rowIndex}-${columnIndex}`}
                    />
                  );
                })}
              </div>
            );
          })}
        </div>
      </Fragment>
    );
  }
}

export default App;
