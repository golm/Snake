import * as React from 'react';

import snakeImage from './snake.png';

const basicStyles = {
  border: '1px solid #eee',
  display: 'inline-block',
  height: '100%',
  width: '1.5em'
};

const activeStyle = {
  backgroundColor: 'salmon'
};

const snakeHeadStyle = {
  backgroundImage: `url(${snakeImage})`,
  backgroundSize: '100%'
};

const Tile = ({ key, active, isApple, isSnakeHead }) => {
  const connectedStyles = {
    ...basicStyles,
    ...(active && !isSnakeHead ? activeStyle : null),
    ...(isSnakeHead ? snakeHeadStyle : null)
  };

  return <span style={connectedStyles}>{isApple ? '🍎' : null} </span>;
};

export default Tile;
